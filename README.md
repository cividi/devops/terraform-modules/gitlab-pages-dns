# cividi Cloudflare GitLab Page Terraform Module

## What is this?

Sets up a DNS entry on Cloudflare for GitLab pages.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale infrastructure. It's provider agnostic. You're encouraged to read the documentation as well as [Terraform: Up and Running](https://www.oreilly.com/library/view/terraform-up-and/9781491977071/). More background can be found in the [Handbook](https://handbook.cividi.io/infrastructure/iac#terraform).

## How to use this module

1. Set up a custom domain for GitLab page and copy the generated verification code (see screenshot below)
   ![GitLab Pages custom domain](/images/gitlab-page-setup.png)
1. Set up a Fathom project with a custom domain, in the format `<name>-cdn.<TLD>`
   ![Fathom custom domain](/images/fathom-setup-01.png)
   ![Fathom custom confirmation](/images/fathom-setup-02.png)
1. Use module in terraform code

   ```hcl
   module "gitlab-pages-dns" "main-domain" {
     source = "gitlab.com/cividi/gitlab-pages-dns/local"
     version = "~> 1.0.0"

     zone = var.zone  # cloudflare zone or use zone_id directly
     name = var.name  # name -> subdomain
     verification_code = var.verification_code  # gitlab pages domain verification code, ideally set via env variable
     fathom_cdn = var.fathom_cdn  # omit or leave empty to disable fathom setup
   }
   ```

### Variables

| name              | as env var                 |  description                       | default               |
| ----------------- | -------------------------- | ---------------------------------- | --------------------- |
| zone              | `TF_VAR_zone`              | dns zone                           |  cividi.io            |
| zone_id           | `TF_VAR_zone_id`           | dns zone id                        |  `None`               |
| name              | `TF_VAR_name`              | page name for subdomain            | `None`                |
| verification_code | `TF_VAR_verification_code` | generated domain verification code | ``                    |
| fathom_cdn        |  `TF_VAR_fathom_cdn`       | generated cdn cname for fathom     | `` -> no fathom entry |
| secondary         |  `TF_VAR_secondary`        | is no primary gitlab pages domain  | `false`               |
| rule_slug         |  `TF_VAR_rule_slug`        | redirect rule slug                 | ``                    |
| rule_dest         |  `TF_VAR_rule_dest`        | redirect rule destination uri      | ``                    |

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md).

## License

See [LICENSE](LICENSE.md)
