terraform {
  required_version = ">= 1.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.20.0"
    }
  }
}

data "cloudflare_zone" "zone" {
  count = var.zone_id == "" ? 1 : 0
  name  = var.zone
}

data "cloudflare_zone" "zone_by_id" {
  count   = var.zone_id == "" ? 0 : 1
  zone_id = var.zone_id
}

locals {
  zone                   = var.zone_id == "" ? var.zone : data.cloudflare_zone.zone_by_id[0].name
  zone_id                = var.zone_id == "" ? data.cloudflare_zone.zone[0].id : var.zone_id
  subdomain              = var.name
  subdomain_verification = "_gitlab-pages-verification-code.${var.name}"
}

resource "cloudflare_record" "gitlab_page_alias" {
  count   = var.verification_code == "" ? 0 : 1
  zone_id = local.zone_id
  name    = local.subdomain
  value   = "cividi.gitlab.io"
  type    = "CNAME"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "gitlab_page_verification" {
  count   = var.verification_code == "" ? 0 : 1
  zone_id = local.zone_id
  name    = local.subdomain_verification
  value   = "gitlab-pages-verification-code=${var.verification_code}"
  type    = "TXT"
}

resource "cloudflare_record" "fathom_custom_domain" {
  count   = var.secondary || var.fathom_cdn == "" ? 0 : 1
  zone_id = local.zone_id
  name    = "${var.name}-cdn"
  value   = var.fathom_cdn
  type    = "CNAME"
}

resource "cloudflare_record" "redirect_dns_proxy" {
  count   = var.rule_slug == "" || var.rule_dest == "" ? 0 : 1
  zone_id = local.zone_id
  name    = "${var.rule_slug}.${var.name}"
  value   = local.zone
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "cloudflare_page_rule" "cloudflare_preview_redirect" {
  count    = var.rule_slug == "" || var.rule_dest == "" ? 0 : 1
  zone_id  = local.zone_id
  target   = "${var.rule_slug}.${var.name}.${local.zone}"
  priority = 1

  actions {
    forwarding_url {
      url         = var.rule_dest
      status_code = 302
    }
  }
}
