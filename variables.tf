variable "zone" {
  type        = string
  description = "tld for bucket custom domain, default: cividi.io"
  default     = "cividi.io"
}

variable "zone_id" {
  type        = string
  description = "DNS zone ID"
  default     = ""
}

variable "name" {
  type        = string
  description = "Name for Page deployment"
}

variable "verification_code" {
  type        = string
  description = "DNS verification code"
  default     = ""
}

variable "fathom_cdn" {
  type        = string
  description = "Fathom CDN"
  default     = ""
}

variable "secondary" {
  type        = bool
  description = "Is an alternate domain"
  default     = false
}

variable "rule_slug" {
  type        = string
  description = "Redirect rule slug"
  default     = ""
}

variable "rule_dest" {
  type        = string
  description = "Redirect rule destination uri"
  default     = ""
}
